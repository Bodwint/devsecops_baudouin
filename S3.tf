resource "aws_s3_bucket" "bucket-baudouin" {
  bucket = "bucket-baudouin"
}

resource "aws_s3_bucket_public_access_block" "bucket-baudouin" {
  bucket = aws_s3_bucket.bucket-baudouin.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_policy" "cf-s3-bucket-policy" {
  bucket = "bucket-baudouin"  # Remplacez par le nom de votre compartiment S3

  policy = <<EOF
{
  "Id": "Policy1707385988517",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1707385986471",
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::bucket-baudouin/*",
      "Principal": "*"
    }
  ]
}
EOF
}


resource "aws_s3_bucket_object" "bucket-baudouin" {
  bucket = aws_s3_bucket.bucket-baudouin.bucket
  key    = "index.html"

  content = <<-EOF
    <!DOCTYPE html>
    <html>
    <head>
      <title>Mon Site Web</title>
    </head>
    <body>
      <h1>Bienvenue sur mon site web!</h1>
    </body>
    </html>
  EOF
}
