### Lancer une instance EC2
resource "aws_instance" "srv-Baudouin" {
	ami = "ami-01d21b7be69801c2f"
	instance_type = "t2.micro"
	tags={Name="ec2-terraform-Baudouin"}
	vpc_security_group_ids = [aws_security_group.instance_bdn.id]
	key_name = "Baudouin-Key"
	user_data = <<-EOF
		#!/bin/bash
		sudo apt-get update
		sudo apt-get install -y apache2
		sudo systemctl start apache2
		sudo systemctl enable apache2
		sudo echo "<h1>Hello devopssec</h1>" > /var/www/html/index.html
		sudo apt-get install -y curl
		sudo DD_API_KEY=23afc71467b71c1122c91317f2cc04ce DD_SITE="datadoghq.eu" bash -c "$(curl -L https://s3.amazonaws.com/dd-agent/scripts/install_script_agent7.sh)"
		sudo systemctl start datadog-agent
	EOF
}

resource "aws_security_group" "instance_bdn" {
    name = "ec2-sg-terraform-baudouin"
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
