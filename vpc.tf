### Créer un VPC 
 resource "aws_vpc" "vpc_baudouin" {
   cidr_block = "172.16.0.0/16"
   tags = {
        Name = "vpc_baudouin"
    }
}

### Création de sous réseaux pour notre VPC
### Sous réseaux privé
resource "aws_subnet" "subnet_private-bdn" {
   vpc_id = aws_vpc.vpc_baudouin.id
   cidr_block = "172.16.1.0/24"
   availability_zone = "eu-west-3a"
   tags = {
        Name = "subnet_private-bdn"
    }
}

### Création de sous réseaux pour notre VPC
### Sous réseaux public
resource "aws_subnet" "subnet_public-bdn" {
   vpc_id = aws_vpc.vpc_baudouin.id
   cidr_block = "172.16.0.0/24"
   availability_zone = "eu-west-3b"
   tags = {
       Name = "subnet_public-bdn"
    }
}

### Créer une passerelle internet
resource "aws_internet_gateway" "igw-baudouin" {
   vpc_id = aws_vpc.vpc_baudouin.id
   tags = {"Name" = "igw-baudouin" }
}

### Créer une nouvelle table de routage
resource "aws_route_table" "route-baudouin" {
   vpc_id = aws_vpc.vpc_baudouin.id
   route {
   cidr_block = "0.0.0.0/0"
   gateway_id = aws_internet_gateway.igw-baudouin.id   
}

   tags = { 
      Name = "table_pub-baudouin"
}
}

### Associer le sous réseau public à la nouvelle table de routage
resource "aws_route_table_association" "table-public-baudouin" {
    subnet_id = aws_subnet.subnet_public-bdn.id
    route_table_id = aws_route_table.route-baudouin.id
}

### Créer une interface réseau
resource "aws_network_interface" "ipaddr-bdn" {
   subnet_id = aws_subnet.subnet_public-bdn.id
   private_ips = ["172.16.0.100"]
}
